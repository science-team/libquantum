Source: libquantum
Section: libs
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Boris Pek <tehnick@debian.org>
Build-Depends: debhelper-compat (= 13)
Homepage: http://www.libquantum.de/
Vcs-Git: https://salsa.debian.org/science-team/libquantum.git
Vcs-Browser: https://salsa.debian.org/science-team/libquantum
Standards-Version: 4.5.0
Rules-Requires-Root: no

Package: libquantum-dev
Section: libdevel
Architecture: any
Depends: libquantum8 (= ${binary:Version}), ${misc:Depends}
Multi-Arch: same
Description: library for the simulation of a quantum computer (development files)
 libquantum is a C library for the simulation of a quantum computer. Based on
 the principles of quantum mechanics, it provides an implementation of a
 quantum register. Basic operations for register manipulation such as the
 Hadamard gate or the Controlled-NOT gate are available through an easy-to-use
 interface. Measurements can be performed on either single qubits or the whole
 quantum register.
 .
 Features:
  * Simulation of arbitrary quantum algorithms is possible
  * High performance and low memory consumption
  * Decoherence support for realistic quantum computation
  * Interface for quantum error correction (QEC)
  * Supports the density operator formalism
  * Implementations of Shor's factoring algorithm and Grover's search
    algorithm are included
 .
 This package contains the header files and static libraries which are needed
 to develop applications based on libquantum.

Package: libquantum8
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: library for the simulation of a quantum computer
 libquantum is a C library for the simulation of a quantum computer. Based on
 the principles of quantum mechanics, it provides an implementation of a
 quantum register. Basic operations for register manipulation such as the
 Hadamard gate or the Controlled-NOT gate are available through an easy-to-use
 interface. Measurements can be performed on either single qubits or the whole
 quantum register.
 .
 Features:
  * Simulation of arbitrary quantum algorithms is possible
  * High performance and low memory consumption
  * Decoherence support for realistic quantum computation
  * Interface for quantum error correction (QEC)
  * Supports the density operator formalism
  * Implementations of Shor's factoring algorithm and Grover's search
    algorithm are included
 .
 This package contains the shared libraries.
